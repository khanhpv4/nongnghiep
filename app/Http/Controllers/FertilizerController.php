<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Fertilizer;

class FertilizerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $fertilizer = Fertilizer::all();
        return response()->json($fertilizer);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $fertilizer = new Fertilizer;
        $fertilizer->name_fertilizer = $request->get('name_fertilizer');
        $fertilizer->link_url = $request->get('link_url');
        $fertilizer->images = $request->get('images');
        $fertilizer->save();
        return response()->json("Thêm thành công");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
          $fetilizer = Fertilizer::find($id);
           return response()->json($fetilizer);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $fetilizer = Fertilizer::find($id);
        return response()->json($fetilizer);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $fertilizer = Fertilizer::find($id);
        $fertilizer->name_fertilizer = $request->get('name_fertilizer');
        $fertilizer->link_url = $request->get('link_url');
        $fertilizer->images = $request->get('images');
        $fertilizer->save();
        return response()->json("Cập nhật thành công");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       $fertilizer = Fertilizer::find($id);
       $fertilizer->delete();
        return response()->json("Xóa thành công");
    }
}
