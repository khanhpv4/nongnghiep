<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Crop;
use App\Model\Category;
use Illuminate\Support\Facades\DB;
class CropController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $crop =  DB::table('crops')
        ->leftJoin('categories', 'categories.id', '=', 'crops.cate_id')
        ->select('crops.*', 'categories.name as cate_name')
        ->get();
        return response()->json($crop);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {


    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // return $request->all();
        $crop = Crop::create($request->all());
        return response()->json("Thêm thành công");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $crop = Crop::find($id);
        return response()->json($crop);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $crop = Crop::find($id);
        $crop->name_nn = $request->get('name_nn');
        $crop->cate_id = $request->get('cate_id');
        $crop->images = $request->get('images');
        $crop->content = $request->get('content');

        $crop->save();
        return response()->json("Update thành công");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $crop = Crop::find($id);
        $crop->delete();
        return response()->json("xóa thành công");
    }

    public function cropCategory() {
        $cate  = Category::all();
        return response()->json($cate);
    }
}
