<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Disease;

class DiseaseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $disease = Disease::all();
        return response()->json($disease);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // return $request->all();
        $disease = new Disease;
        $disease->name_disease= $request->get('name_disease');
        $disease->link_url = $request->get('link_url');
        $disease->images = $request->get('images');
        $disease->save();
        return response()->json("Thêm thành công");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
         $disease = Disease::find($id);
           return response()->json($disease);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       $disease = Disease::find($id);
        return response()->json($disease);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $disease = Disease::find($id);
        $disease->name_disease = $request->get('name_disease');
        $disease->link_url = $request->get('link_url');
        $disease->images = $request->get('images');
        $disease->save();
        return response()->json("Cập nhật thành công");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       $disease = Disease::find($id);
       $disease->delete();
        return response()->json("Xóa thành công");
    }
}
