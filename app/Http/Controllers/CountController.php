<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Crop;
use App\Model\Fertilizer;
use App\Model\User;
use App\User;

class CountController extends Controller
{
    public function nongNghiep()
    {
        $nn = Crop::where('cate_id',1)->count();
        $aq = Crop::where('cate_id',2)->count();
        $cn = Crop::where('cate_id',3)->count();
        return response()->json([
          'nongnghiep' => $nn,
          'congnghiep' => $cn,
          'anqua' => $aq,
        ]);
    }

}
