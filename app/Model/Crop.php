<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Crop extends Model
{
    protected $fillable = [
        'name_nn', 'content', 'images','cate_id'
    ];


}
