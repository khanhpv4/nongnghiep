<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Disease extends Model
{
    protected $fillable = [
        'name_disease', 'link_url', 'images'
    ];
}
