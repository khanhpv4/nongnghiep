<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Fertilizer extends Model
{
   protected $fillable = [
           'name_fertilizer', 'link_url', 'images'
       ];
}
