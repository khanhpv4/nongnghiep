import React, { Component } from "react";
import { Switch, Route, Redirect } from "react-router-dom";
import { Container } from "reactstrap";
import Header from "../../../components/Header/";
import Sidebar from "../../../components/Sidebar/";
import Breadcrumb from "../../../components/Breadcrumb/";
import Aside from "../../../components/Aside/";
import Footer from "../../../components/Footer/";
import { ClipLoader } from "react-spinners";
import {
    Card,
    CardBody,
    CardHeader,
    Col,
    FormGroup,
    Input,
    Label,
    Row,
    Alert,
    FormText
} from "reactstrap";
import Setting from "../../../constant/setting.js";
import CKEditor from "react-ckeditor-component";
class EditDisease extends Component {
    constructor(props) {
        super(props);

        this.state = {
            name_disease: "",
            images: "",
            link_url: "",
            showMyComponent: false,
            showMyEror: false,
            loading: true
        };

        this.handleOnChange = this.handleOnChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }
    componentDidMount() {
        let url =
            Setting.URL +
            "api/disease/" +
            this.props.match.params.id +
            "/edit";
        axios
            .get(url)
            .then(response => {
                this.setState(response.data);
                this.setState({ loading: false });
                console.log(response.data);
            })
            .catch(function(error) {
                console.log(error);
            });
    }

    handleOnChange(e) {
        this.setState({
            [e.target.name]: e.target.value
        });
    }

    async handleSubmit(e) {
        e.preventDefault();

        const data = {
            name_disease: this.state.name_disease,
            link_url: this.state.link_url,
            images: this.state.images
        };
        let url = Setting.URL + "api/disease/" + this.props.match.params.id;
        console.log(url)
        axios
            .patch(url, data)
            .then(response => {
                console.log(response);
                this.setState({
                    showMyComponent: true,
                    loading: false
                });
            })
            .catch(function(error) {
                console.log(error);
            });
    }

    render() {
        const MessagessBox = () => {
            return (
                <Alert
                    color="success"
                    style={
                        this.state.showMyComponent ? {} : { display: "none" }
                    }
                >
                    Create a success alert — check it out!
                </Alert>
            );
        };
        const MessagessEror = () => {
            return (
                <Alert
                    color="danger"
                    style={this.state.showMyEror ? {} : { display: "none" }}
                >
                    Create a error alert — check it out!
                </Alert>
            );
        };
        return (
            <div className="app">
                <Header />
                <div className="app-body">
                    <Sidebar {...this.props} />
                    <main className="main">
                        <Breadcrumb />
                        <Container fluid>
                            <MessagessBox />
                            <MessagessEror />
                            <ClipLoader
                                sizeUnit={"px"}
                                size={50}
                                color={"#39b2d5"}
                                loading={this.state.loading}
                            />

                            <Row>
                                <Col xs="12" sm="12">
                                    <form onSubmit={this.handleSubmit}>
                                        <Card>
                                            <CardHeader>
                                                <strong>Thêm cây trồng</strong>
                                            </CardHeader>
                                            <CardBody>
                                                <Row>
                                                    <Col xs="12">
                                                        <FormGroup>
                                                            <Label htmlFor="name">
                                                                Tên
                                                            </Label>
                                                            <Input
                                                                type="text"
                                                                id="name_disease"
                                                                required
                                                                placeholder="Nhập tên phân bón"
                                                                name="name_disease"
                                                                value={
                                                                    this.state
                                                                        .name_disease
                                                                }
                                                                onChange={
                                                                    this
                                                                        .handleOnChange
                                                                }
                                                            />
                                                        </FormGroup>
                                                    </Col>
                                                    <Col xs="6">
                                                        <FormGroup>
                                                            <Label htmlFor="name">
                                                                Hình Ảnh
                                                            </Label>
                                                            <Input
                                                                type="text"
                                                                id="images"
                                                                required
                                                                placeholder="Nhập url hình ảnh"
                                                                name="images"
                                                                value={
                                                                    this.state
                                                                        .images
                                                                }
                                                                onChange={
                                                                    this
                                                                        .handleOnChange
                                                                }
                                                            />
                                                        </FormGroup>
                                                    </Col>{" "}
                                                    <Col xs="6">
                                                        <FormGroup>
                                                            <Label htmlFor="name">
                                                                Link url
                                                            </Label>
                                                            <Input
                                                                type="text"
                                                                id="link_url"
                                                                required
                                                                placeholder="Nhập url hình ảnh"
                                                                name="link_url"
                                                                value={
                                                                    this.state
                                                                        .link_url
                                                                }
                                                                onChange={
                                                                    this
                                                                        .handleOnChange
                                                                }
                                                            />
                                                        </FormGroup>
                                                    </Col>
                                                    <Col xs="12">
                                                        <button
                                                            className="btn  btn-primary"
                                                            type="submit"
                                                        >
                                                            <i className="fa fa-dot-circle-o" />{" "}
                                                            Update
                                                        </button>
                                                    </Col>
                                                </Row>
                                            </CardBody>
                                        </Card>
                                    </form>
                                </Col>
                            </Row>
                        </Container>
                    </main>
                    <Aside />
                </div>
                <Footer />
            </div>
        );
    }
}

export default EditDisease;
