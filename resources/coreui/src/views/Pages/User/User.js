import React, { Component } from "react";
import { Container } from "reactstrap";
import Header from "../../../components/Header/";
import Sidebar from "../../../components/Sidebar/";
import Breadcrumb from "../../../components/Breadcrumb/";
import Aside from "../../../components/Aside/";
import Footer from "../../../components/Footer/";
import axios from "axios";
import { Link } from "react-router-dom";
import Setting from "../../../constant/setting.js";
import { ClipLoader } from "react-spinners";
import { Card, CardBody, CardHeader, Col, Row, Table } from "reactstrap";

class User extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: true,
            products: "",
            pageCount: 0
        };
        this.handleDelete = this.handleDelete.bind(this);
    }
    componentDidMount() {
        this.callApi();
    }

    callApi() {
        let url = Setting.URL + "api/user";
        axios
            .get(url)
            .then(response => {
                this.setState({ products: response.data, loading: false });
                console.log(this.state.products);
            })
            .catch(function(error) {
                console.log(error);
            });
    }
    handleDelete(e) {
        let id = e;
        let url = Setting.URL + "api/user/" + id;
        if (!confirm("Are your sure you want to delete this item?")) {
            return false;
        }
        axios
            .delete(url)
            .then(response => {
                console.log(response);
                this.callApi();
            })
            .catch(function(error) {
                console.log(error);
            });
    }

    fetchRows() {
        if (this.state.products instanceof Array) {
            return this.state.products.map((object, i) => {
                return (
                    <tr key={i}>
                        <td>{object.id}</td>
                        <td>{object.name}</td>
                        <td>{object.email}</td>
                        <td>
                            <button
                                onClick={() => this.handleDelete(object.id)}
                                className="btn  btn-danger  btn-sm"
                                type="button"
                            >
                                Delete
                            </button>
                        </td>
                    </tr>
                );
            });
        }
    }

    render() {
        return (
            <div className="app">
                <Header />
                <div className="app-body">
                    <Sidebar {...this.props} />
                    <main className="main">
                        <Breadcrumb />
                        <Container fluid>
                            <Row>
                                <Col xs="12" lg="12">
                                    <Card>
                                        <CardHeader>
                                            <i className="fa fa-align-justify" />
                                            Góp ý
                                        </CardHeader>
                                        <CardBody>
                                            <ClipLoader
                                                sizeUnit={"px"}
                                                size={50}
                                                color={"#39b2d5"}
                                                loading={this.state.loading}
                                            />
                                            <Table className="table table-responsive-sm table-bordered table-striped table-sm">
                                                <thead>
                                                    <tr>
                                                        <th>ID</th>
                                                        <th>Tên</th>
                                                        <th>Email</th>
                                                        <th>Delete</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    {this.fetchRows()}
                                                </tbody>
                                            </Table>
                                        </CardBody>
                                    </Card>
                                </Col>
                            </Row>
                        </Container>
                    </main>
                    <Aside />
                </div>
                <Footer />
            </div>
        );
    }
}

export default User;
