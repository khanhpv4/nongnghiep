import React, { Component } from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';
import { Container } from 'reactstrap';
import Header from '../../../components/Header/Header';
import Sidebar from '../../../components/Sidebar/Sidebar';
import Breadcrumb from '../../../components/Breadcrumb/Breadcrumb';
import Aside from '../../../components/Aside/Aside';
import Footer from '../../../components/Footer/Footer';
import { ClipLoader } from 'react-spinners';
import {
    Card,
    CardBody,
    CardHeader,
    Col,
    FormGroup,
    Input,
    Label,
    Row,
    Alert,
    FormText
} from 'reactstrap';
import Setting from '../../../constant/setting.js'
import CKEditor from "react-ckeditor-component";
import { Link } from 'react-router-dom';
class CreateCategory extends Component {
    constructor(props) {
        super(props)

        this.state = {
            name: '',
            showMyComponent: false,
            showMyEror: false,
            loading: false,
        }

        this.handleOnChange = this.handleOnChange.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
    }
    handleOnChange(e) {
        this.setState({
            [e.target.name]: e.target.value
        })
    }
    async handleSubmit(e) {
        e.preventDefault();
        const data = {
            name: this.state.name,
        }
        try {
            const response = await axios.post('/api/category', data);
            console.log(response.data)
            this.setState({
                name: '',
                showMyComponent: true
            }, () => {
            })
        } catch (error) {
            this.setState({
                showMyEror: true
            })
        }
    }



    render() {
        const MessagessBox = () => {
            return (
                <Alert color="success" style={this.state.showMyComponent ? {} : { display: 'none' }}>
                    Create a success alert — check it out!
              </Alert>
            )
        }
        const MessagessEror = () => {
            return (
                <Alert color="danger" style={this.state.showMyEror ? {} : { display: 'none' }}>
                    Create a error alert — check it out!
              </Alert>
            )
        }
        return (
            <div className="app">
                <Header />
                <div className="app-body">
                    <Sidebar {...this.props} />
                    <main className="main">
                        <Breadcrumb />
                        <Container fluid>

                            <MessagessBox />
                            <MessagessEror />
                            <ClipLoader
                                sizeUnit={"px"}
                                size={50}
                                color={'#123abc'}
                                loading={this.state.loading}
                            />


                            <Row>

                                <Col xs="12" sm="12">
                                    <form onSubmit={this.handleSubmit}>
                                        <Card>
                                            <CardHeader>
                                                <strong>Thêm loại cây trồng</strong>
                                            </CardHeader>
                                            <CardBody>
                                                <Row>
                                                    <Col xs="12">
                                                        <FormGroup>
                                                            <Label htmlFor="name">Tên</Label>
                                                            <Input type="text" id="name" required placeholder="Nhập tên cây trồng"
                                                                name="name"
                                                                value={this.state.name}
                                                                onChange={this.handleOnChange}
                                                            />
                                                        </FormGroup>
                                                    </Col>
                                                    <Col xs="12">
                                                            <button className="btn  btn-primary" type="submit"><i className="fa fa-dot-circle-o"></i> Thêm</button>
                                                    </Col>
                                                </Row>

                                            </CardBody>
                                        </Card>
                                    </form>
                                </Col>
                            </Row>
                        </Container>
                    </main>
                    <Aside />
                </div>
                <Footer />
            </div>
        );
    }
}

export default CreateCategory;
