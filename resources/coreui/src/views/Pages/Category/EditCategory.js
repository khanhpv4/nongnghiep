import React, { Component } from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';
import { Container } from 'reactstrap';
import Header from '../../../components/Header/Header';
import Sidebar from '../../../components/Sidebar/Sidebar';
import Breadcrumb from '../../../components/Breadcrumb/Breadcrumb';
import Aside from '../../../components/Aside/Aside';
import Footer from '../../../components/Footer/Footer';
import { ClipLoader } from 'react-spinners';
import {
    Card,
    CardBody,
    CardHeader,
    Col,
    FormGroup,
    Input,
    Label,
    Row,
    Alert,
    FormText
} from 'reactstrap';
import Setting from '../../../constant/setting.js'
class EditCateory extends Component {
    constructor(props) {
        super(props)

        this.state = {
            name: '',
            showMyComponent: false,
            showMyEror: false,
            loading: false,

        }

        this.handleOnChange = this.handleOnChange.bind(this)
        this.handleOnChangeContent = this.handleOnChangeContent.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
    }
    componentDidMount() {
        let url = Setting.URL + 'api/category/' + this.props.match.params.id + '/edit'
        console.log(url)
        axios.get(url)
            .then(response => {
                this.setState(response.data)
                this.setState({ loading: false })
                console.log(response.data)
            })
            .catch(function (error) {
                console.log(error)
            })
    }


    handleOnChange(e) {
        this.setState({
            [e.target.name]: e.target.value
        })
    }
    handleOnChangeContent(e) {
        var newContent = e.editor.getData();
        this.setState({
            content: newContent
        })

    }
    async handleSubmit(e) {
        e.preventDefault();

        const data = {

            name: this.state.name,
        }
        let url = Setting.URL + 'api/category/' + this.props.match.params.id;
        axios.patch(url, data)
            .then(response => {
                console.log(response)
                this.setState({
                    showMyComponent: true,
                    loading: false

                })
            })
            .catch(function (error) {
                console.log(error)
            })
    }

    render() {
        const MessagessBox = () => {
            return (
                <Alert color="success" style={this.state.showMyComponent ? {} : { display: 'none' }}>
                    Create a success alert — check it out!
              </Alert>
            )
        }
        const MessagessEror = () => {
            return (
                <Alert color="danger" style={this.state.showMyEror ? {} : { display: 'none' }}>
                    Create a error alert — check it out!
              </Alert>
            )
        }
        return (
            <div className="app">
                <Header />
                <div className="app-body">
                    <Sidebar {...this.props} />
                    <main className="main">
                        <Breadcrumb />
                        <Container fluid>

                            <MessagessBox />
                            <MessagessEror />
                            <ClipLoader
                                sizeUnit={"px"}
                                size={50}
                                color={'#123abc'}
                                loading={this.state.loading}
                            />
                            <Row>
                                <Col xs="12" sm="12">
                                    <form onSubmit={this.handleSubmit}>
                                        <Card>
                                            <CardHeader>
                                                <strong>Thêm cây trồng</strong>
                                            </CardHeader>
                                            <CardBody>
                                                <Row>
                                                    <Col xs="12">
                                                        <FormGroup>
                                                            <Label htmlFor="name">Tên</Label>
                                                            <Input type="text" id="name" required placeholder="Nhập tên cây trồng"
                                                                name="name"
                                                                value={this.state.name}
                                                                onChange={this.handleOnChange}
                                                            />
                                                        </FormGroup>
                                                    </Col>
                                                    <Col xs="12">
                                                        <button className="btn  btn-primary" type="submit"><i className="fa fa-dot-circle-o"></i>  Sửa</button>
                                                    </Col>
                                                </Row>

                                            </CardBody>
                                        </Card>
                                    </form>
                                </Col>
                            </Row>
                        </Container>
                    </main>
                    <Aside />
                </div>
                <Footer />
            </div>
        );
    }
}

export default EditCateory;
