import React, { Component } from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';
import { Container } from 'reactstrap';
import Header from '../../../components/Header/';
import Sidebar from '../../../components/Sidebar/';
import Breadcrumb from '../../../components/Breadcrumb/';
import Aside from '../../../components/Aside/';
import Footer from '../../../components/Footer/';
import { ClipLoader } from 'react-spinners';
import {
    Card,
    CardBody,
    CardHeader,
    Col,
    FormGroup,
    Input,
    Label,
    Row,
    Alert,
    FormText
} from 'reactstrap';
import Setting from '../../../constant/setting.js'
import CKEditor from "react-ckeditor-component";
class CreateCrop extends Component {
    constructor(props) {
        super(props)

        this.state = {
            name_nn: '',
            content: '',
            images: '',
            cate_id: '',
            showMyComponent: false,
            showMyEror: false,
            loading: false,
            category:'',
        }

        this.handleOnChange = this.handleOnChange.bind(this)
        this.handleOnChangeContent = this.handleOnChangeContent.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
    }
    componentDidMount() {
        this.callCategory();
    }
    async callCategory() {
        let url = Setting.URL + 'api/categoryAll';
        console.log(url)
        axios.get(url)
            .then(response => {
                this.setState({ category: response.data});
                console.log(this.state.category)
            })
            .catch(function (error) {
                console.log(error)
            })

    }

    fetchRows() {
        console.log(this.state.category)
        if (this.state.category instanceof Array) {
            return this.state.category.map((object, i) => {
                return (
                   <option key={i} value={object.id}>{object.name}</option>
                )
            })
        }
    }


    handleOnChange(e) {
        this.setState({
            [e.target.name]: e.target.value
        })
    }
    handleOnChangeContent(e) {
        var newContent = e.editor.getData();
        this.setState({
          content: newContent
        })

    }
    async handleSubmit(e) {
        e.preventDefault();
        const data = {
            name_nn: this.state.name_nn,
            content: this.state.content,
            images: this.state.images,
            cate_id: this.state.cate_id
        }
        try {
            const response = await axios.post('/api/crop', data);
            console.log(response.data)
            this.setState({
                name_nn: '',
                content: '',
                images: '',
                showMyComponent: true
            }, () => {
            })
        } catch (error) {
            this.setState({
                showMyEror: true
            })
        }
    }



    render() {
        const MessagessBox = () => {
            return (
                <Alert color="success" style={this.state.showMyComponent ? {} : { display: 'none' }}>
                    Create a success alert — check it out!
              </Alert>
            )
        }
        const MessagessEror = () => {
            return (
                <Alert color="danger" style={this.state.showMyEror ? {} : { display: 'none' }}>
                    Create a error alert — check it out!
              </Alert>
            )
        }
        return (
            <div className="app">
                <Header />
                <div className="app-body">
                    <Sidebar {...this.props} />
                    <main className="main">
                        <Breadcrumb />
                        <Container fluid>

                            <MessagessBox />
                            <MessagessEror />
                            <ClipLoader
                                sizeUnit={"px"}
                                size={50}
                                color={'#123abc'}
                                loading={this.state.loading}
                            />


                            <Row>

                                <Col xs="12" sm="12">
                                    <form onSubmit={this.handleSubmit}>
                                        <Card>
                                            <CardHeader>
                                                <strong>Thêm cây trồng</strong>
                                            </CardHeader>
                                            <CardBody>
                                                <Row>
                                                    <Col xs="12">
                                                        <FormGroup>
                                                            <Label htmlFor="name">Tên</Label>
                                                            <Input type="text" id="name_nn" required placeholder="Nhập tên cây trồng"
                                                                name="name_nn"
                                                                value={this.state.name_nn}
                                                                onChange={this.handleOnChange}
                                                            />
                                                        </FormGroup>
                                                    </Col>
                                                    <Col xs="12">
                                                        <FormGroup>
                                                            <Label htmlFor="content">Nội Dung</Label>

                                                            <CKEditor
                                                            activeClass="p10"
                                                            content={this.state.content}
                                                            events={{ "change": this.handleOnChangeContent }}
                                                            />
                                                          </FormGroup>
                                                    </Col>

                                                    <Col xs="6">
                                                        <FormGroup>
                                                            <Label htmlFor="name">Hình Ảnh</Label>
                                                            <Input type="text" id="images" required placeholder="Nhập url hình ảnh"
                                                                name="images"
                                                                value={this.state.images}
                                                                onChange={this.handleOnChange}
                                                            />

                                                        </FormGroup>

                                                    </Col>
                                                    <Col xs="6">
                                                        <FormGroup>
                                                            <Label htmlFor="name">Loại cây trồng</Label>
                                                            <Input type="select" value={this.state.cate_id} name="cate_id" id="cate_id" required
                                                                className="form-control"
                                                                onChange={this.handleOnChange}
                                                            >
                                                                <option>Please select</option>
                                                            {this.fetchRows()}
                                                            </Input>
                                                        </FormGroup>
                                                    </Col>

                                                    <Col xs="12">
                                                            <button className="btn  btn-primary" type="submit"><i className="fa fa-dot-circle-o"></i> Thêm</button>
                                                    </Col>
                                                </Row>

                                            </CardBody>
                                        </Card>
                                    </form>
                                </Col>
                            </Row>
                        </Container>
                    </main>
                    <Aside />
                </div>
                <Footer />
            </div>
        );
    }
}

export default CreateCrop;
