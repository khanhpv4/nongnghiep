import React, { Component } from 'react';
import { Container } from 'reactstrap';

import {
    Card,
    CardBody,
    CardHeader,
    Col,
    FormGroup,
    Input,
    Label,
    Row,
    Alert,
    FormText
} from 'reactstrap';
import Setting from '../../constant/setting.js'
class Dashboard extends Component {
    constructor(props) {
        super(props)
        this.state = {  nn: 0, cn: 0, aq: 0 }
        this.callApi()
    }

    callApi(){
        let url = Setting.URL + 'api/count_crop'
        axios.get(url)
            .then(response => {
               this.setState({
                   nn: response.data.nongnghiep,
                   cn: response.data.congnghiep,
                   aq: response.data.anqua,
               })
            })
            .catch(function (error) {
                console.log(error)
            })
    }


    render() {

        return (
            <div className="app">
                <div className="app-body">

                        <Container fluid>
                            <Row>
                            <Col xs="3" sm="6" lg="3">
                            <Card className="text-white bg-primary">
                                <CardBody className="pb-0">
                                    <h1 className="text-value">{ this.state.nn}</h1>
                                    <p><strong>Cây nông nghiêp</strong></p>
                                </CardBody>
                            </Card>
                        </Col>
                        <Col xs="3" sm="6" lg="3">
                            <Card className="text-white bg-info">
                                <CardBody className="pb-0">
                                    <h1 className="text-value">{ this.state.cn}</h1>
                                    <p><strong>Cây công nghiệp</strong></p>
                                </CardBody>
                            </Card>
                        </Col>
                        <Col xs="3" sm="6" lg="3">
                            <Card className="text-white bg-warning">
                                <CardBody className="pb-0">
                                    <h1 className="text-value">{ this.state.aq}</h1>
                                    <p><strong>Cây ăn quả</strong></p>
                                </CardBody>
                            </Card>
                        </Col>
                        <Col xs="3" sm="6" lg="3">
                            <Card className="text-white bg-danger">
                                <CardBody className="pb-0">
                                    <h1 className="text-value">0</h1>
                                    <p><strong>Góp ý</strong></p>
                                </CardBody>
                            </Card>
                        </Col>
                            </Row>
                        </Container>
                </div>
            </div>
        );
    }
}

export default Dashboard;
