export default {
    items: [
        {
            name: 'Dashboard',
            url: '/dashboard',
            icon: 'icon-speedometer',
            badge: {
                variant: 'info',
                text: 'NEW'
            }
        },
        {
            name: 'Cây trồng',
            url: '/crop',
            icon: 'icon-puzzle',

        },
        {
            name: 'Loại cây trồng',
            url: '/category',
            icon: 'icon-puzzle',

        },
        {
            name: 'Phân bón',
            url: '/fertilizer',
            icon: 'icon-puzzle',

        },
        {
            name: 'Bệnh hại',
            url: '/disease',
            icon: 'icon-puzzle',

        },
        {
            name: 'Góp ý',
            url: '/feedback',
            icon: 'icon-puzzle',

        },
        {
            name: 'User',
            url: '/user',
            icon: 'icon-puzzle',

        },
    ]
};
