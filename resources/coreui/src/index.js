import React from 'react';
import ReactDOM from 'react-dom';
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom';

// Styles
// Import Flag Icons Set
import 'flag-icon-css/css/flag-icon.min.css';
// Import Font Awesome Icons Set
import 'font-awesome/css/font-awesome.min.css';
// Import Simple Line Icons Set
import 'simple-line-icons/css/simple-line-icons.css';
// Import Main styles for this application
import '../scss/style.scss'
// Temp fix for reactstrap
import '../scss/core/_dropdown-menu-right.scss'

// Containers
import Full from './containers/Full/'

// Views
import Login from './views/Pages/Login/'
import Register from './views/Pages/Register/'
import Page404 from './views/Pages/Page404/'
import Page500 from './views/Pages/Page500/'
import Crop from './views/Pages/Crop/Crop.js'
import CropCreate from './views/Pages/Crop/CreateCrop.js'
import EditCrop from './views/Pages/Crop/EditCrop.js'
import Fertilizer from './views/Pages/Fertilizer/Fertilizer'
import CreateFertilizer from './views/Pages/Fertilizer/CreateFertilizer.js'
import Editertilizer from './views/Pages/Fertilizer/EditFertilizer.js'
import Category from './views/Pages/Category/Category'
import EditCategory from './views/Pages/Category/EditCategory.js'
import CreateCategory from './views/Pages/Category/CreateCategory'
import Disease from './views/Pages/Disease/Disease'
import CreateDisease from './views/Pages/Disease/CreateDisease.js'
import EditDisease from './views/Pages/Disease/EditDisease.js'
import Feedback from './views/Pages/Feedback/Feedback'
import User from './views/Pages/User/User'

ReactDOM.render((
  <Router>
    <Switch>
      <Route exact path="/login" name="Login Page" component={Login}/>
      <Route exact path="/register" name="Register Page" component={Register}/>
      <Route exact path="/404" name="Page 404" component={Page404}/>
      <Route exact path="/500" name="Page 500" component={Page500}/>
      <Route exact path="/crop" name="Page 500" component={Crop}/>
      <Route exact path="/crop/edit/:id" name="Edit Crop" component={EditCrop}/>
      <Route exact path="/crop/create" name="Crop Create" component={CropCreate}/>
      <Route exact path="/fertilizer" name="Page Fertilizer" component={Fertilizer}/>
      <Route exact path="/fertilizer/create" name="Page CreateFertilizer" component={CreateFertilizer}/>
      <Route exact path="/fertilizer/edit/:id" name="Page Editertilizer" component={Editertilizer}/>
      <Route exact path="/category" name="Page Category" component={Category}/>
      <Route exact path="/category/edit/:id" name="Edit Crop" component={EditCategory}/>
      <Route exact path="/category/create" name="Crop Create" component={CreateCategory}/>
      <Route exact path="/disease" name="Page Desease" component={Disease}/>
      <Route exact path="/disease/create" name="Page CreateDesease" component={CreateDisease}/>
      <Route exact path="/disease/edit/:id" name="Page Desease" component={EditDisease}/>
      <Route exact path="/feedback" name="Page Feedback" component={Feedback}/>
      <Route exact path="/user" name="Page User" component={User}/>
      <Route path="/" name="Home" component={Full}/>
    </Switch>
  </Router>
), document.getElementById('root'));
